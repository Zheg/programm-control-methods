using System;
using System.Collections.Generic;
using lab3;

namespace Lab3MSUnit
{
    [Test]
    public class QueueOnArrayTest
    {
        private MyQueue<int> _queue;
        private Random _random;

        [SetUp]
        public void SetUp()
        {
            _queue = new QueueOnArray<int>();
            _random = new Random();
        }
        
        [TestMethod]
        public void DequeueFromEmptyQueue() {
            Assert.That(() => _queue.Dequeue(), Throws.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void OrderTest() {
            int first = _random.Next();
            int second = _random.Next();
            int third = _random.Next();
            _queue.Enqueue(first);
            _queue.Enqueue(second);
            _queue.Enqueue(third);
            Assert.AreEqual(first, _queue.Dequeue());
            Assert.AreEqual(second, _queue.Dequeue());
            Assert.AreEqual(third, _queue.Dequeue());
            Assert.IsTrue(_queue.Empty());
        }

        [Test]
        [TestCase(1)]
        [TestCase(16)]
        [TestCase(24)]
        [TestCase(32)]
        [TestCase(100)]
        [TestCase(100000)]
        public void BigOrderTest(int size) {
            var items = new int[size];
            for (int i = 0; i < size; i++) {
                items[i] = _random.Next();
                _queue.Enqueue(items[i]);
            }

            for (int i = 0; i < size; i++) {
                Assert.IsFalse(_queue.Empty());
                Assert.AreEqual(items[i], _queue.Dequeue());
            }

            Assert.IsTrue(_queue.Empty());
        }

        [Test]
        [TestCase(1)]
        [TestCase(16)]
        [TestCase(24)]
        [TestCase(32)]
        [TestCase(100)]
        [TestCase(100000)]
        public void MultipleEnqueueDequeue(int repetitions) {
            for (int i = 0; i < repetitions; i++) {
                int item = _random.Next();
                _queue.Enqueue(item);
                Assert.IsFalse(_queue.Empty());
                Assert.AreEqual(item, _queue.Dequeue());
                Assert.IsTrue(_queue.Empty());
            }
        }

        [Test]
        [TestCase(1)]
        [TestCase(16)]
        [TestCase(24)]
        [TestCase(32)]
        [TestCase(100)]
        [TestCase(100000)]
        public void RandomOperationsTest(int repetitions) {
            var items = new List<int>();
            var itemsIndex = 0;
            for (int i = 0; i < repetitions; i++) {
                int operation = _random.Next(4);
                switch (operation) {
                    case 0: {
                        var item = _random.Next();
                        _queue.Enqueue(item);
                        items.Add(item);
                        break;
                    }
                    case 1:
                        if (itemsIndex != items.Count)
                        {
                            Assert.AreEqual( _queue.Dequeue(),  items[itemsIndex++]);
                        }
                        break;
                    case 2: 
                        Assert.AreEqual(_queue.Empty(), itemsIndex == items.Count);
                        break;
                    case 3:
                        Assert.AreEqual(_queue.Count, items.Count - itemsIndex);
                        break;
                    default: 
                        throw new SystemException("Random return value out of bounds");
                }
            }
        }
        
        [TearDown]
        public void TearDown() { }
    }
}
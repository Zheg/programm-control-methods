namespace lab3
{
    public interface Queue<T>
    {
        void Enqueue(T item);
        T Dequeue();
        int Count { get; }
        bool Empty();
    }
}
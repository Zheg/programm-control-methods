using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace lab3
{
    public class QueueOnArray<T> : Queue<T>
    {
        private T[] _array;
        private int _count;
        private int _pointer;

        public QueueOnArray()
        {
            _count = 0;
            _pointer = 0;
            _array = new T[16];
        }

        public void Enqueue(T item)
        {
            if (_count == _array.Length)
            {
                ExtendArray();
            }

            _array[(_pointer + _count++) % _array.Length] = item;
        }

        private void ExtendArray()
        {
            var temp = _array.Clone() as T[];
            Debug.Assert(temp != null, nameof(temp) + " != null");
            _array = new T[_array.Length * 2];
            for (int i = 0; i < temp.Length; i++)
            {
                _array[i] = temp[(i + _pointer) % temp.Length];
            }

            _pointer = 0;
        }

        public T Dequeue()
        {
            if (_count == 0)
            {
                throw new InvalidOperationException();
            }

            T item = _array[_pointer++];
            --_count;
            if (_pointer == _array.Length)
                _pointer = 0;
            return item;
        }

        public int Count => _count;

        public bool Empty()
        {
            return _count == 0;
        }
    }
}